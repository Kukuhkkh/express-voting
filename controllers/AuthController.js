const model = require('../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

module.exports = {
   login: async (req, res) => {
      try {
         const { email, password } = req.body
         const user = await model.users.findOne({where: {
            email
         }})
         if(user.length !== 0) {
            const cek = bcrypt.compareSync(password, user.password)
            if(cek) {
               let token = jwt.sign({id:user.id, email:user.email, nama:user.name}, (process.env.SECRET != '') ? process.env.SECRET : 'sEmp4KBoyyo')
               res.status(200).json({
                  'status': 'ERROR',
                  'messages': 'Login Sukses',
                  'token': token,
                  'data' : {
                     'nama' : user.name,
                     'email' : user.email
                  }
               })
            } else {
               res.status(500).json({
                  'status': 'ERROR',
                  'messages': 'Email atau Password salah'
               })
            }
         } else {
            res.status(500).json({
               'status': 'ERROR',
               'messages': 'Email atau Password salah'
            })
         }
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {}
         })
      }
   },
   register: (req, res) => {

   }
}