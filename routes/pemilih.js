const express = require('express')
const router = express.Router()
const { isAuth, loginToken } = require('../middleware/auth')
const { generatePemilih, loginPemilih, pemilihanKandidat, hapusTokenPemilih, showAll } = require('../controllers/PemilihController')
const { route } = require('.')

router.post('/', isAuth, generatePemilih)
router.post('/:token', loginPemilih)
router.patch('/', loginToken, pemilihanKandidat)
router.delete('/truncate/token', isAuth, hapusTokenPemilih)
router.get('/', isAuth, showAll)

module.exports = router