const model = require('../models/index')
const multer = require('multer')
const path = require('path')
const fs = require('fs')

const storage = multer.diskStorage({
   destination : path.join(__dirname + './../public/images/kandidat'),
   filename: function(req, file, cb){
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));    
   }
})

const upload = multer({
   storage:storage
}).single('avatar');

module.exports = {
   getKandidat: async (req, res) => {
      try{
         const kandidat = await model.kandidat.findAll()
         if(kandidat.length !== 0) {
            res.status(200).json({
               'status': 'OK',
               'messages': '',
               'data': kandidat
             })
         } else {
            res.status(400).json({
               'status': 'ERROR',
               'messages': 'EMPTY',
               'data': {}
             })
         }
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {}
         })
      }
   },
   getKandidatById: async (req, res) => {
      try{
         const kandidat = await model.kandidat.findByPk(req.params.id)
         if(kandidat.length !== 0) {
            res.status(200).json({
               'status': 'OK',
               'messages': '',
               'data': kandidat
             })
         } else {
            res.status(400).json({
               'status': 'ERROR',
               'messages': 'EMPTY',
               'data': {}
             })
         }
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {}
         })
      }
   },
   createKandidat: async (req, res, next) => {
      try {
         await upload(req, res, err => {
            const { nama, visi, misi } = req.body
            validasi()
            async function validasi(){
               let hasil = await model.kandidat.findOne({where: {nama:req.body.nama}})
               if(hasil !== null) {
                  return res.status(500).json({
                     'status': 'ERROR',
                     'messages': 'Nama sudah ada cuy',
                     'code': 1
                  })
               }
            }
            const kandidat = model.kandidat.create({nama,visi,misi,avatar: req.file.filename === undefined ? "" : req.file.filename})
            .then(newKandidat => {
               res.status(201).json({
                  'status': 'OK',
                  'messages': 'Data berhasil ditambahkan',
                  'data': newKandidat
               })
            }).catch(err => {
               fs.unlinkSync(path.join(__dirname + './../public/images/kandidat/' + req.file.filename))
               res.status(500).json({
                  'status': 'ERROR',
                  'messages': err.message,
                  'code': 2
               })
            })
         })
      } catch(err) {
         console.log(`File nama ${req.file}`)
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {},
            'code': 3
         })
      }
   },
   updateKandidat: async (req, res) => {
      try{
         const id = req.params.id
         let oldKandidat
         await model.kandidat.findByPk(id).then(response => {
            oldKandidat = response
         })
         const cek = model.kandidat.findOne(oldKandidat.nama)
         if(cek !== null) {
            return res.status(500).json({
               'status': 'ERROR',
               'messages': 'Nama sudah ada cuy',
               'code': 1
            })
         }
         await upload(req, res, next => {
            const { nama, visi, misi } = req.body
            if(req.file !== undefined) {
               fs.unlinkSync(path.join(__dirname + './../public/images/kandidat/' + oldKandidat.avatar))
               model.kandidat.update({
                  nama,visi,misi,avatar: req.file.filename
               }, { where: { id } })
               .then(() => {
                  res.status(200).json({
                     'status': 'ERROR',
                     'messages': 'Update Sukses'
                  })
               }).catch(err => {
                  fs.unlinkSync(path.join(__dirname + './../public/images/kandidat/' + req.file.filename))
                  res.status(500).json({
                     'status': 'ERROR',
                     'messages': err.message,
                     'code': 2
                  })
               })
            } else {
               model.kandidat.update({
                  nama,visi,misi
               }, { where: { id } })
               .then(newKandidat => {
                  res.status(200).json({
                     'status': 'ERROR',
                     'messages': "image",
                     'kandidat': newKandidat
                  })
               }).catch(err => {
                  fs.unlinkSync(path.join(__dirname + './../public/images/kandidat/' + req.file.filename))
                  res.status(500).json({
                     'status': 'ERROR',
                     'messages': err.message,
                     'code': 2
                  })
               })
            }
         })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {}
         })
      }
   },
   deleteKandidat: async (req, res) => {
      try{
         const id = req.params.id
         const kandidat = await model.kandidat.findByPk(id)
         await model.kandidat.destroy({where: {id}})
         fs.unlinkSync(path.join(__dirname + './../public/images/kandidat/' + kandidat.avatar))
         if(kandidat) {
            res.status(200).json({
               'status': 'OK',
               'message': 'Kandidat Berhasil diHapus'
            })
         }
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {}
         })
      }
   }
}