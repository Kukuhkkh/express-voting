const router = require('express').Router()
const { getUsers, getUser, createUser, updateUser, deleteUser } = require('../controllers/UserController')

// GET users listing.
router.get('/', getUsers)

// POST users
router.post('/', createUser)

// UPDATE users
router.put('/:id', updateUser)

// DELETE users
router.delete('/:id', deleteUser)

// GET user by id
router.get('/:id', getUser)

module.exports = router
