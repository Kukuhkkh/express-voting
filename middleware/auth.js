const jwt = require('jsonwebtoken')

module.exports = {
  isAuth: (req,res,next) => {
    try {
      const token = req.headers.token
      let decoded = jwt.verify(token, process.env.SECRET)
      req.user = decoded
      next()
    } catch(err) {
      res.status(401).json({
        message: 'Token is Invalid'
      })
    }
  },
  loginToken: async (req, res, next) => {
    try {
      const token = req.headers.auth == '' ? 'kosong' : req.headers.auth
      const status = 0
      const model = require('../models/index')
      await model.pemilih.findOne({where:{token:token,status:status}})
            .then(data => {
              req.token = data.token
              next()
            })
            .catch(err => {
              res.status(403).json({
                'message': 'Token auth sudah digunakan atau tidak ada'
              })
            })
    } catch(err) {
      res.status(401).json({
        message: 'Token auth is Invalid'
      })
    }
  }
}
