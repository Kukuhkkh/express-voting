const model = require('../models/index')

module.exports = {
   getAll: async (req, res) => {
      try {
         let labels = []
         let result = []
         await model.sequelize.query(`SELECT COUNT(a.kandidat_id) jumlah, b.nama FROM pemilih a RIGHT JOIN kandidat b on a.kandidat_id=b.id GROUP BY b.id`)
               .then(data => {
                  data[0].forEach((el, id) => {
                     labels.push(el.nama)
                     result.push(el.jumlah)
                  })
                  res.status(200).json({
                     'status': 'OK',
                     'data': {
                        'labels': labels,
                        'datasets': [{
                           label: 'Hasil Perolehan Suara',
                           data: result,
                           backgroundColor: [
                              'rgba(255, 99, 132, 0.2)',
                              'rgba(54, 162, 235, 0.2)',
                              'rgba(255, 206, 86, 0.2)',
                              'rgba(75, 192, 192, 0.2)',
                              'rgba(153, 102, 255, 0.2)',
                              'rgba(255, 159, 64, 0.2)'
                           ],
                           borderColor: [
                              'rgba(255, 99, 132, 1)',
                              'rgba(54, 162, 235, 1)',
                              'rgba(255, 206, 86, 1)',
                              'rgba(75, 192, 192, 1)',
                              'rgba(153, 102, 255, 1)',
                              'rgba(255, 159, 64, 1)'
                           ],
                           borderWidth: 1
                        }]
                     }
                  })
               })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   getAllPemilih: async (req, res) => {
      try {
         await model.pemilih.count()
               .then(data => {
                  res.status(200).json({
                     'status': 'OK',
                     'data': data
                  })
               })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   getPemilihHasVote: async (req, res) => {
      try {
         await model.pemilih.count({where: {status:1}})
               .then(data => {
                  res.status(200).json({
                     'status': 'OK',
                     'data': data
                  })
               })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   getPemilihHasntVote: async (req, res) => {
      try {
         await model.pemilih.count({where: {status:0}})
               .then(data => {
                  res.status(200).json({
                     'status': 'OK',
                     'data': data
                  })
               })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   }
}