'use strict'
const {
  Model
} = require('sequelize')
const bcrypt = require('bcrypt')
const { uuid } = require("uuidv4")
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  users.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {
          msg: "Email must in format foo@bar.com"
        }
      },
      unique: {
        args: true,
        msg: "Email sudah digunakan"
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'users',
  })
  users.beforeCreate(user => {
    user.password = bcrypt.hashSync(user.password, 10),
    user.id = uuid()
  })
  return users
}