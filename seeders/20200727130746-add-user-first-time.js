'use strict';

const { uuid } = require("uuidv4")
const bcrypt = require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('users', [{
     id: uuid(),
     name: 'Kukuh Rahmadani',
     email: 'Krahmadani1@gmail.com',
     password: bcrypt.hashSync('siapsedia',10),
     createdAt: new Date(),
     updatedAt: new Date()
   }, {
    id: uuid(),
    name: 'Angelina Christy',
    email: 'angel@gmail.com',
    password: bcrypt.hashSync('siapsedia',10),
    createdAt: new Date(),
    updatedAt: new Date()
   }], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
