const router = require('express').Router()
const kandidat = require('../controllers/KandidatController')
const {isAuth} = require('../middleware/auth')

router.get('/', kandidat.getKandidat)
router.get('/:id', kandidat.getKandidatById)
router.post('/', isAuth, kandidat.createKandidat)
router.put('/:id', isAuth, kandidat.updateKandidat)
router.delete('/:id', isAuth, kandidat.deleteKandidat)

module.exports = router