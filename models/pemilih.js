'use strict'
const {
  Model
} = require('sequelize')
const { uuid } = require("uuidv4")
module.exports = (sequelize, DataTypes) => {
  class pemilih extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  pemilih.init({
    token: DataTypes.STRING,
    status: DataTypes.INTEGER,
    kandidat_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'pemilih',
    freezeTableName: true
  })
  pemilih.beforeCreate(pemilih => {
    pemilih.id = uuid()
  })
  return pemilih
}