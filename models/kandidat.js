'use strict'
const {
  Model
} = require('sequelize')
const { uuid } = require("uuidv4")
module.exports = (sequelize, DataTypes) => {
  class kandidat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  kandidat.init({
    nama: {
      type: DataTypes.STRING,
      allowNull: false
    },
    visi: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    misi: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    avatar: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'kandidat',
    freezeTableName: true
  })
  kandidat.beforeCreate(kandidat => {
    kandidat.id = uuid()
  })
  return kandidat
}