const model = require('../models/index')
const bcrypt = require('bcrypt')

module.exports = {
   getUsers: async (req, res) => {
      try {
         const users = await model.users.findAll({})
         if (users.length !== 0) {
           res.status(200).json({
             'status': 'OK',
             'messages': '',
             'data': users
           })
         } else {
           res.status(400).json({
             'status': 'ERROR',
             'messages': 'EMPTY',
             'data': {}
           })
         }
       } catch (err) {
         res.status(500).json({
           'status': 'ERROR',
           'messages': err.message,
           'data': {}
         })
       }
   },
   getUser: async (req, res) => {
      try {
         const userID = req.params.id
         const users = await model.users.findOne({where: {
           id: userID
         }})
         if (users.length !== 0) {
           res.status(200).json({
             'status': 'OK',
             'messages': '',
             'data': users
           })
         } else {
           res.status(400).json({
             'status': 'ERROR',
             'messages': 'EMPTY',
             'data': {}
           })
         }
       } catch(err) {
         res.status(500).json({
           'status': 'ERROR',
           'message': err.message,
           'data': {}
         })
       }
   },
   createUser: async (req, res) => {
      try {
         const {
           name, email, password
         } = req.body
         const user = await model.users.create({
           name, email, password
         })
         if(user) {
           res.status(201).json({
             'status': 'OK',
             'messages': 'Data berhasil ditambahkan',
             'data': user
           })
         }
       } catch(err) {
         res.status(400).json({
           'status': 'ERROR',
           'message': err.message
         })
       }
   },
   updateUser: async (req, res) => {
      try {
         const userID = req.params.id
         let {
           name, email, password
         } = req.body
         password = bcrypt.hashSync(password, 10)
         const user = await model.users.update({
           name, email, password
         }, {
           where : {
             id: userID
           }
         })
         if(user) {
           res.status(200).json({
             'status': 'OK',
             'message': 'User Berhasil diUpdate',
             'data': user
           })
         }
       } catch(err) {
         res.status(500).json({
           'status': 'ERROR',
           'message': err.message,
           'data': {}
         })
       }
   },
   deleteUser: async (req, res) => {
      try {
         const userID = req.params.id
         const user = await model.users.destroy({where: {
           id: userID
         }})
         if(user) {
           res.status(200).json({
             'status': 'OK',
             'message': 'User Berhasil diHapus'
           })
         }
       } catch(err) {
         res.status(500).json({
           'status': 'ERROR',
           'message': err.message,
           'data': {}
         })
       }
   }
}