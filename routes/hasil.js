const express = require('express')
const router = express.Router()
const { getAll, getAllPemilih, getPemilihHasVote, getPemilihHasntVote } = require('../controllers/HasilController')

router.post('/', getAll)
router.post('/count-pemilih', getAllPemilih)
router.post('/count-pemilih-vote', getPemilihHasVote)
router.post('/count-pemilih-belum', getPemilihHasntVote)

module.exports = router