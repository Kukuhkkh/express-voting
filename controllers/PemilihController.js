const model = require('../models/index')
const crypto = require('crypto')
const Op = model.Sequelize.Op

const size = 5

const getPagination = (page, size) => {
   const limit = size ? +size : 3
   const offset = page ? page * limit : 0
   
   return { limit, offset }
}

const getPagingData = (data, page, limit) => {
   const { count: totalItem, rows: pemilih } = data
   const currentPage = page ? +page : 0
   const totalPages = Math.ceil(totalItem / limit)
   
   return { totalItem, data:pemilih, totalPages, currentPage }
}

module.exports = {
   showAll: async (req, res) => {
      try {
         const { page, kode } = req.query
         const { limit, offset } = getPagination(page-1, size)
         let condition = kode ? { token: { [Op.like]: `%${kode}%` } } : null

         await model.pemilih.findAndCountAll({where: condition, limit, offset})
            .then(data => {
               const response = getPagingData(data, page, limit)
               res.status(200).json(response)
            })
            .catch(err => {
               res.status(500).json({
                  'status': 'ERROR',
                  'messages': err.message
               })
            })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   generatePemilih: async (req, res) => {
      try {
         const jumlah = req.body.jumlah
         for (let i = 0; i < jumlah; i++) {
            await model.pemilih.create({
               token: crypto.randomBytes(20).toString('hex'),
               status: 0
            })
         }
         res.status(201).json({
            'status': 'OK',
            'messages': 'Token pemilih berhasil digenerate'
         })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   loginPemilih: async (req, res) => {
      try {
         const token = req.params.token
         await model.pemilih.findOne({where:{token}})
            .then(data => {
               if(data.status == 0) {
                  res.status(200).json({
                     'status': 'OK',
                     'messages': 'Token tersedia',
                     'data': data
                  })
               } else {
                  res.status(400).json({
                     'status': 'ERROR',
                     'messages': 'Token sudah digunakan'
                  })
               }
            })
            .catch(err => {
               res.status(400).json({
                  'status': 'ERROR',
                  'messages': err.message
               })
            })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   },
   pemilihanKandidat: async (req, res) => {
      const token = req.token
      const {
         kandidat
      } = req.body
      await model.pemilih.update({
         kandidat_id: kandidat, status: 1
      }, {
         where: {token:token }
      })
      .then(data => {
         res.status(200).json({
            'status': 'OK',
            'messages': 'Token berhasil digunakan'
         })
      })
   },
   hapusTokenPemilih: async (req, res) => {
      try {
         await model.pemilih.destroy({truncate: {cascade:false}})
               .then(() => {
                  res.status(200).json({
                     'status': 'OK',
                     'messages': 'Truncate berhasil'
                  })
               })
               .catch(() => {
                  res.status(500).json({
                     'status': 'ERROR',
                     'messages': 'Gagal truncate'
                  })
               })
      } catch(err) {
         res.status(500).json({
            'status': 'ERROR',
            'messages': err.message
         })
      }
   }
}