'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('kandidat', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true, 
        allowNull: false
      },
      nama: {
        allowNull: false,
        type: Sequelize.STRING(255)
      },
      visi: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      misi: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      avatar: {
        type: Sequelize.STRING(255)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('kandidat');
  }
};